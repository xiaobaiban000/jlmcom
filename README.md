### 1. TVBox 开源版  
- TVBox1.0.0 ，[GitHub社区](https://github.com/CatVodTVOfficial/TVBoxOSC) 根据官方代码仓生成的安卓应用。  
- 移植了猫影视V6的内核，可以无缝对接电视直播、影视剧点播站源规则，就是说原来的接口可以直接用。  
- 本地功能，这个版本也具备了，只需要开启存储权限，在配置地址栏输入本地规则地址即可。  
- 设置——配置地址——输入你的站源规则——确定即可；  
- clan://localhost/tangsan99999/xiaobai.json

### 2. 下载地址
 - [TVbox开源版（俊版）---密123](https://tsq.lanzouf.com/b0c4nr91c#123)   
 - [TVbox2022](https://pan.quark.cn/s/4990bab723a1) 
 - [TVbox本地包](https://pan.quark.cn/s/ee3578306aeb#/list/share) 

### 3. 使用设置  
1). 配置地址---输入站源---确定即可  
- URL站源规则 `https://liu673cn.github.io/box/m.json`  

2). 开启存储权限——将源接口文件放到根目录文件夹下  
-  clan://localhost/tangsan99999/xiaobai.json 



### Github RAW 加速服务
https://www.7ed.net/#/raw-cdn  
https://raw.githubusercontents.com/   用户/仓库/main(分支)/子目录/txt.txt

https://ghproxy.com/  代理  
`https://ghproxy.com/https://raw.githubusercontent.com/liu673cn/box/main/m.json`

`https://raw.iqiq.io/liu673cn/box/main/m.json`

https://doc.fastgit.org/zh-cn/guide.html#ssh-操作  
`https://raw.fastgit.org/`

`https://raw-gh.gcdn.mirr.one/`

### Github 静态加速  
`https://cdn.staticaly.com/gh/liu673cn/box/main/m.json`  

`https://cdn.jsdelivr.net/gh/liu673cn/box@main/m.json`  

`https://purge.jsdelivr.net/gh/`  

https://purge.jsdelivr.net/gh/liu673cn/box@main/m.json  强制刷新

--------
### 以上为互联网流传资源，不保证内容的真实性和可靠性。本页面只是收集，自用请勿宣传。
